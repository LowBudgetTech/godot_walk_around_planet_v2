# godot_walk_around_planet_v2
Instructions:
1. Load Godot project by double clicking on project.godot
2. Run example scene by pressing the play button
3. Use WASD to move around and SPACE to jump
4. Keep pressing SPACE to jump again while in the air (try to fly to the other planet!)

Video link for code explanation: https://youtu.be/aL8TB_mB3j8

5. Please let me know if you have any problems!
